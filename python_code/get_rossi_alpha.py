'''
Description: Plot Ross-alpha based on list of time stamps
Author: Ming Fang
Date: 2021-09-11 23:53:54
LastEditors: Ming Fang
LastEditTime: 2023-06-08 10:52:53
'''
import numpy as np
import matplotlib.pyplot as plt
import sys
from pathlib import Path
# from numba import njit
from scipy.optimize import curve_fit
# plt.style.use(['science', 'no-latex','grid'])


# @njit
def getTimeDist(lst):
    tmax = 300.0
    nbin = 300
    w = tmax / nbin
    bins = np.linspace(0, tmax, nbin+1)
    counts = np.zeros(nbin)
    N = len(lst)
    for i in range(N - 1):
        if i % 1000000 == 0:
            print(i)
        t_end = lst[i] + tmax
        # get time distribution of pulses within (t_start, t_end)
        for j in range(i+1, N):
            if lst[j] < t_end:
                dt = lst[j] - lst[i]
                binIndex = int(np.floor(dt / w))
                counts[binIndex] = counts[binIndex] + 1
            else:
                break
    return [counts, bins]


def model(x, a, b, c):
    """Rossi-alpha model

    Args:
        x (float): time difference in us
        a (float): amplitude
        b (float): decay rate
        c (float): background

    Returns:
        float: [description]
    """
    return a * np.exp(-x / b) + c


def my_exponential_fit(xdata, ydata):
    n = len(xdata)
    S = np.zeros(n)
    for k in range(1, n):
        S[k] = S[k-1] + 0.5 * (ydata[k]+ydata[k-1]) * (xdata[k]-xdata[k-1])
    # solve the regression system: V = M U
    K = np.array([xdata-xdata[0], S])
    M = np.matmul(K, K.T)
    V = np.matmul(K, ydata - ydata[0])
    U = np.linalg.solve(M, V)
    c2 = U[1]
    c2 = -1/22.32
    theta = np.exp(c2 * xdata)
    K = np.array([np.ones(n), theta])
    M = np.matmul(K, K.T)
    V = np.matmul(K, ydata)
    P = np.linalg.solve(M, V)
    return [P[1], -1/c2, P[0]]


def fit_Rossi_alpha(xdata, ydata):
    popt, pcov = curve_fit(model, xdata, ydata/np.max(ydata))
    # popt = my_exponential_fit(xdata, ydata/np.max(ydata))
    print("best fit parameters = ", popt)

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.scatter(xdata, ydata, s=4, label='Simulated data')
    ax.plot(xdata, np.max(ydata)*model(xdata, *popt), 'r',
            label=r'Fit: die-away time=%5.2f$\pm$%5.2f$\mu$s' % tuple([popt[1], np.sqrt(np.diag(pcov))[1]]))
    # ax.plot(xdata, np.max(ydata)*model(xdata, *popt), 'r',
    #         label=r'Fit: die-away time=%5.2f $\mu$s' % tuple([popt[1]]))

    ax.set_xlim([0, 300])
    # ax.set_ylim([0.5, 5.8])
    ax.set_xlabel('Time (us)')
    ax.set_ylabel('Count rate (cps)')
    ax.legend()
    plt.show()


if __name__ == "__main__":
    timestamp_file = Path(sys.argv[1]) / "mergedtimestamps.npy"
    timestamps = np.load(timestamp_file) # list of timestamps in us
    print(len(timestamps))
    TME = timestamps[-1] / 1e6 # seconds
    counts, bins = getTimeDist(timestamps)
    centers = (bins[1:] + bins[:-1]) / 2  # us
    # np.savetxt(Path(sys.argv[1])/'rossialpha.txt', np.transpose([centers, counts]), fmt='%.5f %d')
    fit_Rossi_alpha(centers, counts/TME)
