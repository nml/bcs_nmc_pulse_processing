'''
Description: Example of using the paser
Author: Ming Fang
Date: 2022-08-22 20:55:13
LastEditors: Ming Fang
LastEditTime: 2023-06-07 19:14:02
'''
from pathlib import Path
import numpy as np
from matplotlib import pyplot as plt
from WaveBinFile import WaveBinFile
from numba import njit


def get_pulse_heights(voltagePulses, method=0, window_len=11, window='hanning'):
    if method == 0:
        return np.max(voltagePulses, axis=1)
    else:
        if window == 'flat': #moving average
            w=np.ones(window_len,'d')
        else:
            w=eval('np.'+window+'(window_len)')
        return get_filtered_pulse_heights(voltagePulses, window_len, w/w.sum())


@njit
def get_filtered_pulse_heights(voltagePulses, window_len, w):
    phs = np.zeros(len(voltagePulses))
    index=0
    n = len(voltagePulses[0])
    k = window_len-1
    for p in voltagePulses:
        s = np.zeros(k+n+k)
        s[:k] = p[k:0:-1]
        s[k:k+n] = p
        s[k+n:] = p[-2:-k-2:-1]
        y = np.zeros(n+k)
        for i in range(n+k):
            for j in range(k):
                y[i] += w[k-j] * s[i+j]
        filtered_p = y[int(window_len/2):-int(window_len/2)]
        # if np.max(p) > 0.01:
        #     fig, ax = plt.subplots(1, 1, figsize=(6,6))
        #     ax.plot(p, label="Raw")
        #     ax.plot(filtered_p, label="Smoothed")
        #     ax.legend()
        #     plt.show()
        phs[index] += np.max(filtered_p)
        index += 1
    return phs


def pulse2str(p):
    a = [p['Board'], p['Channel'], p['Time Stamp'], p['Energy'], p['Energy Short'], p['Flags'], p['Number of Samples']]
    a += list(p['Samples'])
    return ','.join([str(x) for x in a])


def extract_timestamp_analog_channel(filepath, version: int, threshold: float, save2disk: bool = False):
    # load binary waveform file
    bin_files_handle = WaveBinFile(Path(filepath), version=version)
    totalN = bin_files_handle.totalNumberOfPulses
    print(
        "Version {0} analog test data. Board number {1}, Channel number {2}. There are {3} pulses."
        .format(bin_files_handle.versionNumber, bin_files_handle.boardNumber,
                bin_files_handle.channelNumber, totalN))

    # extract pulse height and time stamp
    BASELINENUM = 16
    VMAX = 2.0
    NBITS = 14
    POLARITY = 1
    coeff = VMAX / (2**NBITS - 1)
    chunkSize = 1000000
    pulseHeights = np.zeros(totalN, dtype=float)
    timeStamps = np.zeros(totalN, dtype=np.int64)
    eventsRead = 0
    while bin_files_handle.numberOfPulsesUnread > 0:
        pulses = bin_files_handle.readNextNPulses(chunkSize)
        samples = pulses['Samples']
        baseLines = np.mean(samples[:, :BASELINENUM], axis=1)
        voltagePulses = coeff * (samples - baseLines[:,None]) * POLARITY
        pulseHeights_chunk = get_pulse_heights(voltagePulses, method=1)
        timeStamps_chunk = pulses['Time Stamp']
        pulseHeights[eventsRead:(eventsRead+len(pulseHeights_chunk))] = pulseHeights_chunk
        timeStamps[eventsRead:(eventsRead+len(pulseHeights_chunk))] = timeStamps_chunk
        eventsRead = eventsRead+len(pulseHeights_chunk)
        print("{0} pulses processed, {1} pulses left".format(eventsRead, totalN-eventsRead))
    # determine if the pulse crosses the threshold
    # save time stamp of pulses that cross the threhold
    cross_threshold = pulseHeights > threshold
    if save2disk:
        output_directory = Path('output_py/analog')
        output_directory.mkdir(parents=True, exist_ok=True)
        # np.savetxt(output_directory/f"timestamps_{CH}", timeStamps, fmt='%d')
        np.save(output_directory/f"timestamps_{CH}", timeStamps[cross_threshold])

    fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    ax.hist(pulseHeights, bins=500, range=(0, 0.5))
    ax.set_xlabel('Pulse height (V)')
    ax.set_ylabel('Counts')
    ax.set_title("Pulse height distribution")
    plt.show()
    return timeStamps[cross_threshold]

def extract_timestamp_ttl_channel(filepath, version: int, save2disk: bool = False):
    # load binary waveform file
    bin_files_handle = WaveBinFile(Path(filepath), version=version)
    totalN = bin_files_handle.totalNumberOfPulses
    print(
        "Version {0} TTL test data. Board number {1}, Channel number {2}. There are {3} pulses."
        .format(bin_files_handle.versionNumber, bin_files_handle.boardNumber,
                bin_files_handle.channelNumber, totalN))

    # extract time stamp
    chunkSize = 1000000
    timeStamps = np.zeros(totalN, dtype=np.int64)
    eventsRead = 0
    while bin_files_handle.numberOfPulsesUnread > 0:
        pulses = bin_files_handle.readNextNPulses(chunkSize)
        timeStamps_chunk = pulses['Time Stamp']
        timeStamps[eventsRead:(eventsRead+len(timeStamps_chunk))] = timeStamps_chunk
        eventsRead = eventsRead+len(timeStamps_chunk)
        print("{0} pulses processed, {1} pulses left".format(eventsRead, totalN-eventsRead))
    if save2disk:
        output_directory = Path('output_py/ttl')
        output_directory.mkdir(parents=True, exist_ok=True)
        # np.savetxt(output_directory/f"timestamps_{CH}", timeStamps, fmt='%d')
        np.save(output_directory/f"timestamps_{CH}", timeStamps)

    # fig, ax = plt.subplots(1, 1, figsize=(6, 6))
    # ax.hist(pulseHeights, bins=500, range=(0, 0.5))
    # ax.set_xlabel('Pulse height (V)')
    # ax.set_ylabel('Counts')
    # ax.set_title("Pulse height distribution")
    # plt.show()
    return timeStamps

def mergeSortedArray(tts):
    arrayN = len(tts)
    if arrayN == 1:
        return tts[0]
    lengths = [len(i) for i in tts]
    totalItemN = np.sum(lengths)
    indices = np.zeros(arrayN, dtype=np.int64)
    mergedArray = []
    # flags = np.array([False, False, False, False, False], dtype=np.bool8)
    for i in range(totalItemN):
        curMinVal = 1E18
        curMinIdx = -1
        for j in range(arrayN):
            if indices[j] >= lengths[j]:
                continue
            if curMinVal < tts[j][indices[j]]:
                continue
            else:
                curMinIdx = j
                curMinVal = tts[j][indices[j]]
        # mergedArray.append([curMinIdx + 1, curMinVal]) # save channel num and timestamp
        mergedArray.append(curMinVal)
        indices[curMinIdx] += 1
    return np.array(mergedArray)


if __name__ == "__main__":
    data_type = "analog"  # or "ttl"
    if data_type == "analog":
        thresholds = [0.014, 0.0143, 0.0125, 0.0125, 0.0113, 0.0123]
        filepath_t = 'testdata/analog/DataR_CH{}@DT5730_1770_ananlog_100MB.BIN'
    else:
        filepath_t = 'testdata/ttl/DataR_CH{}@DT5730_1770_ttl_100MB.BIN'
    channel_numbers = [1, 2, 3, 4, 5, 6]
    timestamps_all_channels = []
    for i, CH in enumerate(channel_numbers):
        filepath = filepath_t.format(CH)
        if data_type == "analog":
            timestamps_single_channel = extract_timestamp_analog_channel(filepath, 2, thresholds[i])
        else:
            timestamps_single_channel = extract_timestamp_ttl_channel(filepath, 2)
        timestamps_all_channels.append(timestamps_single_channel)
    mergedTimestamps = mergeSortedArray(timestamps_all_channels)
    assert len(mergedTimestamps) == np.sum([len(i) for i in timestamps_all_channels]), "Before merge: {} items; After merge: {} items".format(len(mergedTimestamps), np.sum([len(i) for i in timestamps_all_channels]))
    assert all(mergedTimestamps[:-1] <= mergedTimestamps[1:]), "Merged array is not sorted."
    print(len(mergedTimestamps))
    output_directory = Path('output_py') / data_type
    output_directory.mkdir(parents=True, exist_ok=True)
    np.save(output_directory/'mergedtimestamps', mergedTimestamps/1e6) # ps to us, binary
    # np.savetxt(output_directory/'mergedtimestamps', mergedTimestamps/1e6, "%.8f") # ps to us, text
