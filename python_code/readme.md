Python scripts that do the same thing as the matlab ones.

## How to run the test
1. Move all python scripts to the outer directory.
2. Run `extract_time_stamp.py` in terminal.
    A numpy file `mergedtimestamps.npy` is generated in folder `output_py/analog`.
2. Run `get_rossi_alpha.py output_py/analog` in terminal.
    The Rossi-alpha plot and the fit show up on your screen. The die-away time found thorugh the fit is 22.3 us.
3. Run `python get_SDT.py output_py/analog 22.33` in terminal.
    The S,D,T count rates and their associated uncertainties are printed, as shown below:
    ```
        Single (cps): 1798.4315, sigma_s (cps): 2.2768
        Double (cps): 260.0158, sigma_D (cps): 1.2486
        Triple (cps): 21.2912, sigma_T (cps): 0.62511
    ```