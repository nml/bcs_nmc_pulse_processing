'''
Description: Calculate S, D, T and associated uncertainties based on list of time stamps
Author: Ming Fang
Date: 2021-09-11 23:53:54
LastEditors: Ming Fang
LastEditTime: 2023-06-07 19:45:37
'''
import numpy as np
# import matplotlib.pyplot as plt
import sys
from pathlib import Path
from scipy.special import comb
# from numba import njit
# from numba.typed import List
# plt.style.use(['science', 'no-latex','grid'])


# @njit
def getMultiDist(lst, predelay=2, gate=48, longdelay=2000):
    """Generate the real+accidental (R+A) and accidental (A) distribution.

    Args:
        lst (list[float]): list of timestamps in us.
        predelay (int, optional): Pre-delay in us. Defaults to 2.
        gate (int, optional): Gate width in us. Defaults to 48.
        longdelay (int, optional): Long delay in us. Defaults to 2000.

    Returns:
        [list[int], list[int]]: First return value is the distribution of multiplicity counts within real+accidental gate, i.e., (pre-delay, pre-delay + gate), 
        second return value is the distribution of multiplicity counts within accidental gate, i.e., (long-delay, long-delay + gate).
    """
    N = len(lst)
    multi_ra = np.zeros(maxMultiplicity)
    multi_a = np.zeros(maxMultiplicity)
    skip_until = -1
    for i in range(N - 1):
        if lst[i] <= skip_until:
            continue
        t_ra_start = lst[i] + predelay
        t_ra_end = t_ra_start + gate
        ra_counts = 0
        # get number of pulses within (t_ra_start, t_ra_end)
        for j in range(i+1, N):
            if lst[j] <= t_ra_start:
                continue
            elif lst[j] >= t_ra_end:
                break
            else:
                ra_counts = ra_counts + 1
        # get number of pulses within (t_a_start, t_a_end)
        t_a_start = t_ra_end + longdelay
        t_a_end = t_a_start + gate
        a_counts = 0
        for j in range(i+1, N):
            if lst[j] <= t_a_start:
                continue
            elif lst[j] >= t_a_end:
                break
            else:
                a_counts = a_counts + 1
        # print cases where there are more than 10 events in the R+A gate
        # if so, skip 100 us
        if ra_counts > 20:
            print(i, ra_counts, lst[i])
            skip_until = lst[i] + 100
            continue
        multi_ra[ra_counts] = multi_ra[ra_counts] + 1
        multi_a[a_counts] = multi_a[a_counts] + 1
    return [multi_ra, multi_a]


def calculate_SDT_rates(Pn, Qn, N, TME):
    """Implementation based on 

    Args:
        Pn (array[int]): Multiplicity distribution in R+A gate
        Qn (array[int]): Multiplicity distribution in A gate
        N (int): Max multiplicity
        TME (float): Total measurement time, second.

    Returns:
        array[float]: Singles, doubles, triples count rates.
    """
    S = len(timestamps) / TME
    D = np.dot(np.arange(N), Pn-Qn) / TME
    t = np.dot(np.arange(N)*(np.arange(N)-1)/2, Pn-Qn)
    cross_correlation_term = np.dot(np.arange(N), Qn) / sum(Qn) * np.dot(np.arange(N), Pn-Qn)
    T = (t - cross_correlation_term)/TME
    return [S, D, T]


def calculate_SDT_uncertainties(S, D, T, gate, TME):
    """Implementation based on 

    Args:
        S (float): Singles count rate, cps
        D (float): Doubles count rate, cps
        T (float): Triples count rate, cps
        gate (float): Gate width, us
        TME (float): Total measurement time, second
    
    Returns:
        array[float]: Uncertainty associated with singles, doubles, triples count rates.
    """
    def get_w(k: int):
        if k < 1:
            return 0
        w = 0
        for j in range(k-1):
            if j == 0:
                w+=1
                continue
            w += comb(k-1, j)*(-1)**j*(1-np.exp(-j*lmbd_TG))/(j*lmbd_TG)
        return w

    def get_f(k: int):
        if k < 1:
            return 0
        if k <= 1:
            return 1
        return pow(np.exp(-lmbd_Tp)*(1-np.exp(-lmbd_TG)), k-1)
    
    tau  = die_away_time # 21.80
    lmbd = 1/tau
    lmbd_TG = lmbd*gate
    lmbd_Tp = lmbd*predaley
    wks = np.array([get_w(k) for k in range(0, 4)])
    fks = np.array([get_f(k) for k in range(0, 4)])
    # print(wks, fks)
    xks = wks / fks
    gate *= 1e-6  # seconds
    Ss               = np.sqrt((S+2*D*xks[2])/TME)
    Ds               = np.sqrt((D+2 * S**2 * gate+2*T+4*D*S*gate*xks[2]+(2* D**3 * xks[2]/S**2))/TME)
    Ts = (T+D**2*gate+D*S*gate+2*S*T*gate+S**3*gate**2+2*D*T**2*xks[2]/S**2+10*D**2*gate*xks[2])/TME
    Ts += (2*D**3*gate*xks[2]/S+2*D*S*gate*xks[2]+4*D*T*gate*xks[2]+4*D*S**2*gate**2*xks[2]+4*D**2*S*gate**2*xks[2]**2)/TME
    Ts += (12*D*T*gate*xks[3]+12*S*T*gate*xks[3]+0+0+0)/TME
    Ts = np.sqrt(Ts)
    return [Ss, Ds, Ts]


if __name__ == "__main__":
    timestamp_file = Path(sys.argv[1]) / "mergedtimestamps.npy"
    die_away_time = float(sys.argv[2]) # die away time in us
    predaley = 3 #us
    gate = 48 #us
    longdelay = 2000 #us
    maxMultiplicity = 256

    timestamps = np.load(timestamp_file) # list of timestamps in us
    print(len(timestamps))
    TME = timestamps[-1] / 1e6 # seconds

    # get multiplicity distribution in R+A gate, A gate
    multi_ra, multi_a = getMultiDist(timestamps, predelay=predaley, gate=gate, longdelay=longdelay)
    # np.savetxt(dir/'multiplicity.txt', np.transpose([multi_ra, multi_a]), fmt='%d %d')

    # calculate S, D, T count rates (unit: cps)
    S, D, T = calculate_SDT_rates(multi_ra, multi_a, maxMultiplicity, TME)

    # calculate uncertainties associated with S, D, T rates (unit: cps)
    Ss, Ds, Ts = calculate_SDT_uncertainties(S, D, T, gate, TME)
    print("Single (cps): {0:.5f}, sigma_s (cps): {1:.5f}, Double (cps): {2:.5f}, sigma_D (cps): {3:.5f}, Triple (cps): {4:.5f}, sigma_T (cps): {5:.5f}".format(S,Ss,D,Ds,T,Ts))
    # np.savetxt(dir/"Single_double_triple_rate.txt", np.array([S,Ss,D,Ds,T,Ts]), fmt='%.5f', header='Single(cps)\tsigma_s(cps)\tDouble(cps)\tsigma_D(cps)\tTriple(cps)\tsigma_T(cps)')
