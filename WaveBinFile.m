classdef WaveBinFile < handle
    properties
        versionNumber  % CoMPASS version number
        numberOfSamplesPerPulse  % number of samples per pulse
        boardNumber  % board number
        channelNumber  % channel number
        totalNumberOfPulses  % total number of pulses in the file
        numberOfPulsesUnread  % number of pulses that have not been read
        headersize  % header size in bytes
    end
    properties (Access = private)
        filePath  % file path
        pulseType  % custom data type for pulse object
        pulseSize  % pulse size in bytes
        fileObject  % data file object
    end
    
    methods
        function obj = WaveBinFile(p, version)
            % Constructor
            % Initialize with input file path and version number.
            
            % p: Path to the binary data file saved by CoMPASS.
            % version: Version of the CoMPASS software. Must be 1 or 2.
            
            % Raise error if the input file is not accessible
            if ~exist(p, 'file')
                error('%s is not accessible.', p);
            end
            
            % Raise error if the version number is incorrect
            if version ~= 1 && version ~= 2
                error('Version number must be either 1 or 2, but got %d.', version);
            end
            
            obj.filePath = p;
            obj.versionNumber = version;
            obj.headersize = 24;
            
            if version == 2
                obj.headersize = obj.headersize + 1;
            end
            
            obj.getCommonHeader();
            
            obj.fileObject = fopen(obj.filePath, 'rb');
            
            if version == 2
                % skip first 2 bytes
                fseek(obj.fileObject, 2, 'cof');
            end
            
            obj.pulseSize = obj.headersize + 2 * obj.numberOfSamplesPerPulse;
            s = dir(obj.filePath);
            filesize = s.bytes;
            obj.totalNumberOfPulses = floor(filesize / obj.pulseSize);
            obj.numberOfPulsesUnread = obj.totalNumberOfPulses;
            
            if version == 1
                obj.pulseType = struct(...
                    'Board', int16(0), ...
                    'Channel', int16(0), ...
                    'TimeStamp', int64(0), ...
                    'Energy', int16(0), ...
                    'Energy_Short', int16(0), ...
                    'Flags', int32(0), ...
                    'Number_of_Samples', int32(0), ...
                    'Samples', uint16(0));
            else
                obj.pulseType = struct(...
                    'Board', int16(0), ...
                    'Channel', int16(0), ...
                    'TimeStamp', int64(0), ...
                    'Energy', int16(0), ...
                    'Energy_Short', int16(0), ...
                    'Flags', int32(0), ...
                    'Waveform_Code', int8(0), ...
                    'Number_of_Samples', int32(0), ...
                    'Samples', uint16(0));
            end
        end
        
        function close(obj)
            % Destructor
            fclose(obj.fileObject);
        end
        
        function skipNextPulse(obj)
            % Skip the next pulse
            fseek(obj.fileObject, obj.pulseSize, 'cof');
            obj.numberOfPulsesUnread = obj.numberOfPulsesUnread - 1;
        end
        
        function skipNextNPulses(obj, num)
            % Skip the next N pulses
            fseek(obj.fileObject, obj.pulseSize * num, 'cof');
            obj.numberOfPulsesUnread = obj.numberOfPulsesUnread - num;
            
            if obj.numberOfPulsesUnread < 0
                obj.numberOfPulsesUnread = 0;
            end
        end
    
        function pulse = readNextPulse(obj)
            % Read the next pulse
            if obj.numberOfPulsesUnread > 0
                buffer = fread(obj.fileObject, obj.pulseSize, 'uint8');
                pulse = obj.parsePulse(buffer);
                obj.numberOfPulsesUnread = obj.numberOfPulsesUnread - 1;
            else
                pulse = [];
            end
        end

        function pulses = readNextNPulses(obj, num)
            % Read the next N pulses
            if obj.numberOfPulsesUnread > 0
                if obj.numberOfPulsesUnread < num
                    num = obj.numberOfPulsesUnread;
                end
                buffer = fread(obj.fileObject, obj.pulseSize * num, 'uint8');
                pulses = obj.parsePulses(buffer, num);
                obj.numberOfPulsesUnread = obj.numberOfPulsesUnread - num;
            else
                pulses = [];
            end
        end

        % function rewind(obj)
        %     % Go to the beginning of the file
        %     fseek(obj.fileObject, 0, 'bof');
        %     obj.numberOfPulsesUnread = obj.totalNumberOfPulses;
        % end
    end

    methods (Access = private)
        function getCommonHeader(obj)
            % Read common headers of all pulses
            fileID = fopen(obj.filePath, 'rb');
            if obj.versionNumber == 2
                fseek(fileID, 2, 'cof');
            end
            obj.boardNumber = fread(fileID, 1, 'int16');
            obj.channelNumber = fread(fileID, 1, 'int16');
            if obj.versionNumber == 1
                fseek(fileID, 16, 'cof');
            else
                fseek(fileID, 17, 'cof');
            end
            obj.numberOfSamplesPerPulse = fread(fileID, 1, 'int32');
            fclose(fileID);
        end

        function pulse = parsePulse(obj, buffer)
            % Parse a single pulse from the buffer
            pulse = obj.pulseType;
            idx = 1;
            pulse.Board = typecast(uint8(buffer(idx:idx+1)), 'int16');
            idx = idx + 2;
            pulse.Channel = typecast(uint8(buffer(idx:idx+1)), 'int16');
            idx = idx + 2;
            pulse.TimeStamp = typecast(uint8(buffer(idx:idx+7)), 'int64');
            idx = idx + 8;
            pulse.Energy = typecast(uint8(buffer(idx:idx+1)), 'int16');
            idx = idx + 2;
            pulse.Energy_Short = typecast(uint8(buffer(idx:idx+1)), 'int16');
            idx = idx + 2;
            pulse.Flags = typecast(uint8(buffer(idx:idx+3)), 'int32');
            idx = idx + 4;

            if obj.versionNumber == 2
                pulse.Waveform_Code = typecast(uint8(buffer(idx)), 'int8');
                idx = idx + 1;
            end

            pulse.Number_of_Samples = typecast(uint8(buffer(idx:idx+3)), 'int32');
            idx = idx + 4;

            numSamples = pulse.Number_of_Samples;
            tmp = idx+2*numSamples-1;
            pulse.Samples = typecast(uint8(buffer(idx:tmp)), 'uint16');
        end

        function pulses = parsePulses(obj, buffer, num)
            % Parse multiple pulses from the buffer
            pulses = repmat(obj.pulseType, num, 1);

            for i = 1:num
                pulseBuffer = buffer((i-1)*obj.pulseSize+1:i*obj.pulseSize);
                pulses(i) = obj.parsePulse(pulseBuffer);
            end
        end
    end
end
