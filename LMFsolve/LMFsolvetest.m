% LMFsolvetest	Test of the function LMFtest
% ~~~~~~~~~~~~  { default = Rosenbrock }   2009-01-08

% x   = inp('Starting point [xo]',[-1.2; 1]);
x = input('Starting point [xo]') ;
if isempty(x)
    x = [-1.2; 1];
end
x   = x(:);
lx  = length(x);
% fun = inp('Function name      ','rosen');
fun = input('Function name     ') ;
if isempty(fun)
    fun = 'rosen';
end
% D   = eval(inp('vektor vah      [D]','1'));
D = input('vektor vah      [D]') ;
if isempty(D)
    D = 1;
end
% ipr = inp('Print control   ipr',1);
ipr = input('Print control   ipr') ;
if isempty(ipr)
    ipr = 1;
end

resid = eval(['@' fun]);    %   function handle
options = LMFsolve('default');
options = LMFsolve...
    (options,...
    'XTol',1e-6,...
    'FTol',1e-12,...
    'ScaleD',D,...
    'Display',ipr...
    );
[x,S,cnt]=LMFsolve(resid,x,options);
%         ~~~~~~~~~~~~~~~~~~~~~~~~~

fprintf('\n  radius = %15.8e\n', sqrt(x'*x));

