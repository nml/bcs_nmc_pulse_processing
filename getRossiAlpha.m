%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script reads the timestamps saved previously and gets the die-away time
% through data fitting.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% User input starts
load('output_matlab/analog/mergedtimestamps.mat'); % Load required data
%%% User input ends

% Execute the main script
disp(numel(mergedTimestamps_us));
TME = mergedTimestamps_us(end) / 1e6; % seconds
[counts, bins] = getTimeDist(mergedTimestamps_us);
centers = (bins(2:end) + bins(1:end-1)) / 2;

fit_Rossi_alpha(@model, centers(:), counts(:)./TME);

% Define functions
function [counts, bins] = getTimeDist(lst)
    tmax = 300.0;
    nbin = 300;
    w = tmax / nbin;
    bins = linspace(0, tmax, nbin+1);
    counts = zeros(1, nbin);
    N = numel(lst);
    for i = 1:(N-1)
        if mod(i, 1000000) == 0
            disp(i);
        end
        t_end = lst(i) + tmax;
        for j = (i+1):N
            if lst(j) < t_end
                dt = lst(j) - lst(i);
                binIndex = floor(dt / w) + 1;
                counts(binIndex) = counts(binIndex) + 1;
            else
                break;
            end
        end
    end
end

function y = model(p, xdata)
    % Rossi-alpha model
    y = p(1) * exp(-xdata ./ p(2)) + p(3);
end

function fit_Rossi_alpha(model, xdata, ydata)
    % Curve fitting requring Optimization Toolbox
%     popt = lsqcurvefit(@model, [1, 20, 0.1], xdata, ydata./max(ydata));
    popt_init = my_init_fit(xdata, ydata./max(ydata));
    popt = LMF_fit(@model, xdata, ydata./max(ydata), popt_init);
    disp(['best fit parameters = ', num2str(popt)]);

    fig = figure;
    ax = axes('Parent', fig);
    scatter(ax, xdata, ydata, 'filled', 'MarkerFaceColor', 'b', 'MarkerEdgeColor', 'b');
    hold(ax, 'on');
    plot(ax, xdata, max(ydata).*model(popt, xdata), 'r', 'LineWidth', 1.5);
    hold(ax, 'off');

    xlabel(ax, 'Time (us)');
    ylabel(ax, 'Count rate (cps)');
    legend(ax, 'Simulated data', sprintf('Fit: die-away time=%5.2f µs', popt(2)));
    xlim(ax, [0, 300]);
end

function popt = my_init_fit(xdata, ydata)
% Perform regression of functions of the form y = a + b exp(c x).
%
% Syntax: popt = my_exponential_fit(xdata, ydata)
%
% Ref: https://scikit-guess.readthedocs.io/en/latest/appendices/reei/translation.html#reei2
    n = length(xdata);
    S = zeros(n, 1);
    for k = 2:n
        S(k) = S(k-1) + 0.5 * (ydata(k)+ydata(k-1)) * (xdata(k) - xdata(k-1));
    end
    M11 = dot(xdata-xdata(1), xdata-xdata(1));
    M12 = dot(xdata-xdata(1), S);
    M21 = M12;
    M22 = dot(S, S);
    M_inv = 1/(M11*M22-M21*M12) .* [M22 -M12; -M21 M11];
    V1 = dot(ydata-ydata(1), xdata-xdata(1));
    V2 = dot(ydata-ydata(1), S);
    V = [V1; V2];
    U = M_inv * V;
    c2 = U(2);
    theta = exp(c2 * xdata);
    N11 = n;
    N12 = sum(theta);
    N21 = N12;
    N22 = dot(theta, theta);
    N_inv = 1/(N11*N22-N21*N12) .* [N22 -N12; -N21 N11];
    K = [sum(ydata); dot(ydata, theta)];
    P = N_inv * K;
    popt = [P(2) -1/c2 P(1)];
end

function popt = LMF_fit(model, xdata, ydata, init_guess)
% Perform non-linear least-square fit using the Levenberg-Marquardt-Fletcher algorithm.
%
% Syntax: popt = LMF_fit(model, xdata, ydata, init_guess)
%
    addpath('./LMFsolve');
    D = 1;
    ipr = 0; % print control of interation results
%     resid = eval(['@' fun]);    %   function handle
    resid = @(p) model(p, xdata) - ydata;
    options = LMFsolve('default');
    options = LMFsolve...
        (options,...
        'XTol',1e-6,...
        'FTol',1e-12,...
        'ScaleD',D,...
        'Display',ipr...
        );
    [x,S,cnt]=LMFsolve(resid, init_guess, options);
    disp(['sum of residual squares = ', num2str(S)]);
    %         ~~~~~~~~~~~~~~~~~~~~~~~~~
    popt = x';
end
