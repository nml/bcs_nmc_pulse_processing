%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script reads the timestamps saved previously and calculates Singles, 
% Doubles, Triples count rates and their associated uncertainties using a 
% signal-trigger shift register algorithm.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% User input starts
load('output_matlab/analog/mergedtimestamps.mat'); % Load required data
die_away_time = 22.33; % die away time in us
predelay = 3; % us
gate = 48; % us
longdelay = 2000; % us
maxMultiplicity = 256;
%%% User input ends

disp(numel(mergedTimestamps_us));
TME = mergedTimestamps_us(end) / 1e6; % seconds

% Perform calculations
[multi_ra, multi_a] = getMultiDist(mergedTimestamps_us, maxMultiplicity, predelay, gate, longdelay);
[S, D, T] = calculate_SDT_rates(multi_ra, multi_a, maxMultiplicity, TME);
[Ss, Ds, Ts] = calculate_SDT_uncertainties(S, D, T, die_away_time, predelay, gate, TME);

% Display results
disp(['Single (cps): ', num2str(S), ', sigma_s (cps): ', num2str(Ss)]);
disp(['Double (cps): ', num2str(D), ', sigma_D (cps): ', num2str(Ds)]);
disp(['Triple (cps): ', num2str(T), ', sigma_T (cps): ', num2str(Ts)]);

% Define functions
function [multi_ra, multi_a] = getMultiDist(lst, maxMultiplicity, predelay, gate, longdelay)
    N = numel(lst);
    multi_ra = zeros(1, maxMultiplicity);
    multi_a = zeros(1, maxMultiplicity);
    skip_until = -1;
    for i = 1:(N-1)
        if lst(i) <= skip_until
            continue;
        end
        t_ra_start = lst(i) + predelay;
        t_ra_end = t_ra_start + gate;
        ra_counts = 0;
        for j = (i+1):N
            if lst(j) <= t_ra_start
                continue;
            elseif lst(j) >= t_ra_end
                break;
            else
                ra_counts = ra_counts + 1;
            end
        end
        t_a_start = t_ra_end + longdelay;
        t_a_end = t_a_start + gate;
        a_counts = 0;
        for j = (i+1):N
            if lst(j) <= t_a_start
                continue;
            elseif lst(j) >= t_a_end
                break;
            else
                a_counts = a_counts + 1;
            end
        end
        % skip multiplicities counts > 20
        if ra_counts > 20
            disp([i, ra_counts, lst(i)]);
            skip_until = lst(i) + 100;
            continue;
        end
        multi_ra(ra_counts+1) = multi_ra(ra_counts+1) + 1;
        multi_a(a_counts+1) = multi_a(a_counts+1) + 1;
    end
end

function [S, D, T] = calculate_SDT_rates(Pn, Qn, N, TME)
    % Santi, Peter A., et al. China Center of Excellence Nondestructive Assay Training Course. 
    % No. LA-UR-14-21691. Los Alamos National Lab.(LANL), Los Alamos, NM (United States), 2014.
    % p.93-96.
    S =(sum(Pn)+1) / TME;
    D = dot(0:(N-1), Pn-Qn) / TME;
    t = dot((0:(N-1)).*((0:(N-1))-1)/2, Pn-Qn);
    cross_correlation_term = dot(0:(N-1), Qn) / sum(Qn) * dot(0:(N-1), Pn-Qn);
    T = (t - cross_correlation_term) / TME;
end

function [Ss, Ds, Ts] = calculate_SDT_uncertainties(S, D, T, tau, predelay, gate, TME)
    % Ref: Prasad, M. K., N. J. Snyderman, and J. M. Verbeke. 
    %      "Analytical error bars and RSD for neutron multiplicity counting." 
    %      NIMA 903 (2018): 25-31.
    lmbd = 1/tau;
    lmbd_TG = lmbd*gate;
    lmbd_Tp = lmbd*predelay;
    wks = zeros(1, 4);
    fks = zeros(1, 4);
    for k = 0:3
        wks(k+1) = get_w(k);
        fks(k+1) = get_f(k);
    end
    xks = wks ./ fks;
    gate = gate * 1e-6; % seconds
    Ss = sqrt((S+2*D*xks(3)) / TME);
    Ds = sqrt((D+2*S^2*gate+2*T+4*D*S*gate*xks(3)+(2*D^3*xks(3))/S^2) / TME);
    Ts = (T+D^2*gate+D*S*gate+2*S*T*gate+S^3*gate^2+2*D*T^2*xks(3)/S^2+10*D^2*gate*xks(3)) / TME;
    Ts = Ts + (2*D^3*gate*xks(3)/S+2*D*S*gate*xks(3)+4*D*T*gate*xks(3)+4*D*S^2*gate^2*xks(3)+4*D^2*S*gate^2*xks(3)^2) / TME;
    Ts = Ts + (12*D*T*gate*xks(4)+12*S*T*gate*xks(4)) / TME;
    Ts = sqrt(Ts);
    
    function w = get_w(k)
        if k < 1
            w = 0;
            return;
        end
        w = 0;
        for j = 0:(k-2)
            if j == 0
                w = w + 1;
                continue;
            end
            w = w + nchoosek(k-1, j)*(-1)^j*(1-exp(-j*lmbd_TG))/(j*lmbd_TG);
        end
    end

    function f = get_f(k)
        if k < 1
            f = 0;
            return;
        end
        if k <= 1
            f = 1;
            return;
        end
        f = (exp(-lmbd_Tp)*(1-exp(-lmbd_TG)))^(k-1);
    end
end
