%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This script reads the CoMPASS binary data and save the event timestamps to a 
% mat file.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% User input starts
data_type = 'analog';  % or 'ttl'
if strcmp(data_type, 'analog')
    % for analog data, a voltage threshold should be provided to reject low amplitude events.
    thresholds = [0.014, 0.0143, 0.0125, 0.0125, 0.0113, 0.0123];
    % file name pattern
    filepath_t = 'testdata/analog/DataR_CH%d@DT5730_1770_ananlog_100MB.BIN';
else
    filepath_t = 'testdata/ttl/DataR_CH%d@DT5730_1770_ttl_100MB.BIN';
end
channel_numbers = [1, 2, 3, 4, 5, 6];
CoMPASS_version = 2; % either 1 or 2
save_each_channel = false; % save timestamps of each channel
plot_first_100_pulses = true; % show first 100 pulses
plot_pulse_height_distribution = true; % show pulse height distribution
%%% User input ends


output_directory = ['output_matlab/' data_type];
mkdir(output_directory);
timestamps_all_channels = cell(1, length(channel_numbers));
% extract timestamps channel by channel
for i = 1:length(channel_numbers)
    CH = channel_numbers(i);
    filepath = sprintf(filepath_t, CH);
    if strcmp(data_type, 'analog')
        [timestamps_single_channel, pulseHeights, first100Pulses] = read_data_per_channel(filepath, CoMPASS_version, data_type, thresholds(i));
    else
        [timestamps_single_channel, pulseHeights, first100Pulses] = read_data_per_channel(filepath, CoMPASS_version, data_type, 0);
    end
    if save_each_channel
        save([output_directory '/timestamps_' num2str(CH)], 'timeStamps');
    end
    if plot_first_100_pulses
        plotPulses(first100Pulses, CH);
    end
    if plot_pulse_height_distribution
        plotPulseHeightDistribution(pulseHeights, CH);
    end
    
    timestamps_all_channels{i} = timestamps_single_channel;
end
% merge timestamps from different channels into a single sorted array
mergedTimestamps = mergeSortedArray(timestamps_all_channels);
assert(length(mergedTimestamps) == sum(cellfun(@length, timestamps_all_channels)), 'Before merge: %d items; After merge: %d items', length(mergedTimestamps), sum(cellfun(@length, timestamps_all_channels)));
assert(all(mergedTimestamps(1:end-1) <= mergedTimestamps(2:end)), 'Merged array is not sorted.');
% disp(length(mergedTimestamps));
% save to file
format long
mergedTimestamps_us = double(mergedTimestamps) / 1e6;
save([output_directory '/mergedtimestamps.mat'], 'mergedTimestamps_us');

function pulseHeights = get_pulse_heights(voltagePulses, method, window_len, window)
    if method == 0
        pulseHeights = max(voltagePulses, [], 2);
    else
        if strcmp(window, 'flat')
            w = ones(window_len, 1);
        else
            % w = hann(window_len);
            w = zeros(window_len, 1);
            for n = 1:window_len
                w(n) = 0.5 * (1 - cos(2*pi*(n-1)/(window_len-1)));
            end
        end
        pulseHeights = get_filtered_pulse_heights(voltagePulses, window_len, w / sum(w));
    end
end

function phs = get_filtered_pulse_heights(voltagePulses, window_len, w)
    phs = zeros(size(voltagePulses, 1), 1);
    index = 1;
    n = size(voltagePulses, 2);
    k = window_len - 1;
    for i = 1:size(voltagePulses, 1)
        p = voltagePulses(i, :);
        s = zeros(1, k + n + k);
        s(1:k) = p(k+1:-1:2);
        s(k + 1:k + n) = p;
        s(k + n + 1:end) = p(end - 1:-1:end - k);
        y = conv(s(:),w,'valid');
        filtered_p = y(floor(window_len / 2) + 1:end - floor(window_len / 2));
        phs(index) = max(filtered_p);
        index = index + 1;
    end
end

function str = pulse2str(p)
    a = [p.Board, p.Channel, p.TimeStamp, p.Energy, p.EnergyShort, p.Flags, p.NumberOfSamples];
    a = [a p.Samples];
    str = sprintf('%d,', a);
    str(end) = '';
end

function [timeStamps, pulseHeights, first100pulses] = read_data_per_channel(filepath, version, data_type, threshold)
    % Read pulse time stamps, pulse heights from CoMPASS binary file.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Pulse heights are calculated in the following way:
    %   a. Calculate the baseline, which is the average of the first 16 samples of the waveform.
    %   b. Subtract the baseline.
    %   c. Multiply every sample with 2.0/(2^14-1) to get the sampling value in Volt.
    %   d. If data_type is 'analog', apply a filter to remove spikes.
    %   e. Calculate the max of all samples, which is the pulse height.
    %   f. If data_type is 'analog', accept the pulse if its height is above the threshold; otherwise reject it.
    %
    % [timeStamps, pulseHeights, first100pulses] = read_data_per_channel(filepath, version, data_type, threshold)
    % filepath      is the path to the binary file,
    % version       is the major version number of CoMPASS software. Must be either 1 or 2,
    % data_type     is either "analoig" or "ttl" depending on the output ports.
    % threshold     is the voltage threshold in Volt to reject low-amplitude pulses. Used only when data_type is "analog",
    %
    % Output Arguments:
    % timestamps    event time stamps in picoseconds.
    % pulseHeights  pulse height in Volts.
    % first100pulses    first 100 pulses, only for plotting.
    % 
    bin_files_handle = WaveBinFile(filepath, version);
    totalN = bin_files_handle.totalNumberOfPulses;
    fprintf('Version %d test data. Board number %d, Channel number %d. There are %d pulses.\n', bin_files_handle.versionNumber, bin_files_handle.boardNumber, bin_files_handle.channelNumber, totalN);

    BASELINENUM = 16;
    VMAX = 2.0;
    NBITS = 14;
    POLARITY = 1;
    coeff = VMAX / (2^NBITS - 1);
    chunkSize = 1000000; % read 1,000,000 pulses each time
    
    first100pulses = zeros(100, bin_files_handle.numberOfSamplesPerPulse);
    pulseHeights = zeros(totalN, 1);
    timeStamps = zeros(totalN, 1, 'int64');
    eventsRead = 0;
    nSamples = bin_files_handle.numberOfSamplesPerPulse;
    while bin_files_handle.numberOfPulsesUnread > 0
        pulses = bin_files_handle.readNextNPulses(chunkSize);
        samples = zeros(length(pulses), nSamples);  % Preallocate the array
        for i = 1:length(pulses)
            samples(i,:) = pulses(i).Samples;
        end
        baseLines = mean(samples(:, 1:BASELINENUM), 2);
        voltagePulses = coeff * (samples - baseLines) * POLARITY;
        if eventsRead < 100
            n = min(100, length(pulses));
            first100pulses(1:n, :) = voltagePulses(1:n, :);
        end
        if strcmp(data_type, 'analog')
            pulseHeights_chunk = get_pulse_heights(voltagePulses, 1, 11, 'hanning');
        else
            pulseHeights_chunk = get_pulse_heights(voltagePulses, 0, 1, ' ');
        end
        timeStamps_chunk = arrayfun(@(event) event.TimeStamp, pulses);
        pulseHeights(eventsRead + 1 : eventsRead + length(pulseHeights_chunk)) = pulseHeights_chunk;
        timeStamps(eventsRead + 1 : eventsRead + length(pulseHeights_chunk)) = timeStamps_chunk;
        eventsRead = eventsRead + length(pulseHeights_chunk);
        fprintf('%d pulses processed, %d pulses left\n', eventsRead, totalN - eventsRead);
    end
    
    if strcmp(data_type, 'analog')        
        cross_threshold = pulseHeights > threshold;
        timeStamps = timeStamps(cross_threshold);
        pulseHeights = pulseHeights(cross_threshold);
    end
end

function plotPulses(pulses, CH)
    nSamples = size(pulses, 2);
    disp(['Pulses, channel ' num2str(CH)]);
    figure;
    plot(2*(0:(nSamples-1)), pulses);
    ylim([0 0.5]);
    xlabel('Time (ns)');
    ylabel('Voltage (V)');
    title(['Pulses, channel ' num2str(CH)]);
    grid on;
end

function plotPulseHeightDistribution(pulseHeights, CH)
    disp(['Pulse height distribution, channel ' num2str(CH)]);
    figure;
    histogram(pulseHeights, 'BinWidth', 0.001, 'BinLimits', [0, 0.5], 'DisplayStyle','stairs');
    xlabel('Pulse height (V)');
    ylabel('Counts');
    title(['Pulse height distribution, channel ' num2str(CH)]);
    grid on;
end
function mergedArray = mergeSortedArray(tts)
    arrayN = length(tts);
    if arrayN == 1
        mergedArray = tts{1};
        return;
    end

    lengths = cellfun(@length, tts);
    totalItemN = sum(lengths);
    indices = zeros(arrayN, 1);
    mergedArray = zeros(totalItemN, 1, 'int64');
    for i = 1:totalItemN
        curMinVal = 1e18;
        curMinIdx = -1;
        for j = 1:arrayN
            if indices(j) >= lengths(j)
                continue;
            end
            if curMinVal < tts{j}(indices(j) + 1)
                continue;
            else
                curMinIdx = j;
                curMinVal = tts{j}(indices(j) + 1);
            end
        end
        mergedArray(i) = curMinVal;
        indices(curMinIdx) = indices(curMinIdx) + 1;
    end
end
