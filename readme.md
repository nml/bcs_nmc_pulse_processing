# Matlab scripts to extract timestamps from CoMPASS data and calculate die-away time, S, D, T
## Folder contents:
    .
    ├── readme.md: This file.
    ├── WaveBinFile.m: Matlab script to parse binary file saved by CoMPASS.
    ├── extractTimeStamp.m: Extract timestamps from binary data.
    ├── getRossiAlpha.m: Calculate Rossi-alpha distribution and perform exponential fit to extract the die-away time. 
    ├── getSDT.m: Matlab script to calculate Singles, Doubles, Triples count rates and their associated uncertainties using a signal-trigger shift register algorithm. Multiplicity > 20 will be printed and rejected.
    ├── testdata
    │   │   ├── analog
    │   │   │   └── DataR_CH*@DT5730_1770_ananlog_100MB.BIN: Data acquired from anaglog channel in a Cf-252 measurement. Truncated to reduce size to 100MB.
    |   |   ├── ttl
    │   │   │   └── DataR_CH*@DT5730_1770_ttl_100MB.BIN:     Data acquired from ttl channel in a Cf-252 measurement. Truncated to reduce size to 100MB.
    ├── LMFsolve: external library for performing non-linear least-square fit.
    └── unittest: scripts and data for unit testing.

## Dependencies

No Matalab Toolbox is needed.

## How to run the test:

1. Run `extractTimeStamp.m`
    A mat file `mergedtimestamps.mat` is generated in folder `output_matlab/analog`.
2. Run `getRossiAlpha.m`
    The Rossi-alpha plot and the fit show up on your screen. The die-away time found through data-fitting is 22.3 us.
3. Run `getSDT.m`
    The S,D,T count rates and their associated uncertainties are printed, as shown below:
    ```
        Single (cps): 1798.4315, sigma_s (cps): 2.2768
        Double (cps): 260.0158, sigma_D (cps): 1.2486
        Triple (cps): 21.2912, sigma_T (cps): 0.62511
    ```
Change the input section of each script when applying them to process data from new measurements. Run the scripts in the same order as in the test.

## How to run the unittest (not required):

1. Move `TestBinParserV1.m` and `TestBinParserV2.m` to the outer directory. Run `runtests` in matlab and the following output should be printed:
    ```
        Totals:
        8 Passed, 0 Failed, 0 Incomplete.
        1.3037 seconds testing time.
    ```